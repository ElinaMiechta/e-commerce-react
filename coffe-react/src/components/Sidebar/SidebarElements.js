import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {FaTimes} from 'react-icons/fa';

export const SidebarContainer = styled.aside`
position:fixed;
z-index:999;
width: 350px;
height:100%;
background: #ca9b72;
display:grid;
align-items:center;
top:0;
transition: 0.3s ease-out;
right:${({isOpen}) => (isOpen ? '0' : '-1000px')};

@media only screen and(max-width:400px) {
      width:100%;
}
`;

export const CloseIcon = styled(FaTimes)`
color: #000;
`;

export const Icon = styled.div`
position:absolute;
top:1.2rem;
right:1.5rem;
background:transparent;
border:transparent;
font-size: 2rem;
cursor:pointer;
outline:none;
`;

export const SidebarMenu = styled.div`
display:grid;
grid-template-column-column:1fr;
grid-template-rows: repeat(3,80px);
text-align:center;


@media only screen and(max-width:480px) {
      grid-template-rows: repeat(3,60px);
`;

export const SidebarLink = styled(Link)`
display:flex;
align-items:center;
justify-content:center;
font-size:1.5rem;
text-decoration:none;
list-style:none;
transition: .2s ease-out;
text-decoration:none;
color: #000;
cursor:pointer;

&:hover {
      color: #b14503;
      transition: .2s ease-out;
}
`;

export const SidebarBtnWrap = styled.div`
display:flex;
justify-content:center;
`;

export const SidebarRoute = styled(Link)`
background: #633206;
white-space:no-wrap;
padding: 16px 64px;
color: #fff;
font-size:16px;
outine:none;
border:none;
cursor:pointer;
transition: .2s ease-out;
text-decoration:none;

&:hover {
      transition: .2s ease-out;  
      background: #fff;
      color: #010606;
}
`;