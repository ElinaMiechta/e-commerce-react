import React from 'react';
import {SidebarContainer
,Icon
,CloseIcon
,SidebarMenu
,SidebarLink
,SidebarBtnWrap
,SidebarRoute} 
from './SidebarElements';

function Sidebar(props) {
      return (
                  <SidebarContainer isOpen={props.isOpen} onClick={props.toggle}>
                        <Icon onClick={props.toggle}>
                              <CloseIcon/>
                        </Icon>
                        <SidebarMenu>
                              <SidebarLink to="/">Cups</SidebarLink>
                              <SidebarLink to="/">Deserts</SidebarLink>
                              <SidebarLink to="/">Full Menu</SidebarLink>
                        </SidebarMenu>
                        <SidebarBtnWrap>
                              <SidebarRoute to="/">Order now</SidebarRoute>
                        </SidebarBtnWrap>

                  </SidebarContainer>
                  
      )
}

export default Sidebar
