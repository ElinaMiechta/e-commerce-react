import React from 'react';
import {FeatureContainer, FeatureButton} from './FeatureElements';

const Feature = () => {
      return (
            <FeatureContainer>
                  <h1>Coffe Best Seller</h1>
                  <p>Caramel Brulée Frappuccino - espresso, milk and ice blended with rich caramel brulée sauce, and then topped with whipped cream and more rich sauce for extra-gooey goodness. </p>
   <FeatureButton>Order now</FeatureButton>
                  
            </FeatureContainer>
      )
}

export default Feature
