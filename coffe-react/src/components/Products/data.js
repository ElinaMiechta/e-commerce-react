import product1 from '../../images/single-cup.jpg';
import product2 from '../../images/capuchino.jpg';
import product3 from '../../images/carmel-latte.jpg';
import product4 from '../../images/mocachino.jpg';
import desert1 from '../../images/desert1.jpg';
import desert2 from '../../images/desert2.jpg';
import desert3 from '../../images/desert3.jpg';

export const productData = [
      {
            img:product1,
            alt: 'coffe',
            name: 'Latte Machiatto',
            desc: 'A latte macchiato often uses only half an espresso shot or less.',
            price: '$9.99',
            button: 'Add to Cart'
      },
      {
            img:product2,
            alt: 'coffe',
            name: 'Latte Machiatto',
            desc: 'A latte macchiato often uses only half an espresso shot or less.',
            price: '$9.99',
            button: 'Add to Cart'
      },
      {
            img:product3,
            alt: 'coffe',
            name: 'Latte Machiatto',
            desc: 'A latte macchiato often uses only half an espresso shot or less.',
            price: '$9.99',
            button: 'Add to Cart'
      },
      {
            img:product4,
            alt: 'coffe',
            name: 'Latte Machiatto',
            desc: 'A latte macchiato often uses only half an espresso shot or less.',
            price: '$9.99',
            button: 'Add to Cart'
      }
];

export const productData2 = [
      {
            img:desert1,
            alt: 'cookie',
            name: 'Home made cookie',
            desc: 'Natural ingridients home made cookie',
            price: '$4.99',
            button: 'Add to Cart'
      },
      {
            img:desert2,
            alt: 'cookie',
            name: 'Home made cookie',
            desc: 'Natural ingridients home made cookie',
            price: '$3.99',
            button: 'Add to Cart'
      },
      {
            img:desert3,
            alt: 'cookie',
            name: 'Home made cookie',
            desc: 'Natural ingridients home made cookie',
            price: '$3.99',
            button: 'Add to Cart'
      }
]