import React,{useState} from 'react';
import Navbar from '../Navbar';
import {LandingContainer, LandingContent,LandingItems,
      LandingH1,LandingP,LandingBtn} 
      from './LandingElements';
import Sidebar from '../Sidebar';

function Landing() {
const [isOpen,setisOpen] = useState(false);
const toggle = () => {
      setisOpen(!isOpen);
};

      return (
            <LandingContainer>
                  <Navbar toggle={toggle}/>
                  <Sidebar isOpen={isOpen} toggle={toggle}/>
                  <LandingContent>
                        <LandingItems>
                              <LandingH1>Craft Coffe in the Town</LandingH1>
                              <LandingP>Get your cup of coffe in 10 minutes</LandingP>
                              <LandingBtn>Get a Cup</LandingBtn>
                        </LandingItems>
                  </LandingContent>
            </LandingContainer>

                  
           
      )
}

export default Landing
