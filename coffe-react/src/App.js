import './App.css';
import Navbar from './components/Navbar';
import {BrowserRouter as Router} from 'react-router-dom';
import {GlobalStyle} from './globalStyles';
import Landing from './components/Landing';
import { productData, productData2 } from './components/Products/data';
import Products from './components/Products';
import Feature from './components/Feature';
import Footer from './components/Footer';

function App() {
  return (
    <Router>
      <GlobalStyle/>
      <Landing/>
      <Products heading='Choose your favourite' data={productData}/>
      <Feature />
      <Products heading='What about cookie?' data={productData2}/>
      <Footer/>
    </Router>

  );
}

export default App;
